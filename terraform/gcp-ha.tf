variable "consul_count" {
  description = "Number of consul nodes to create"
  default     = 3
}

variable "database_count" {
  description = "Number of database nodes to create"
  default     = 3
}

variable "application_count" {
  description = "Numer of application nodes to create"
  default     = 1
}

variable "prefix" {
  description = "Namespace resources will be created with.  Example: rclamp will create rclamp-consul-0"
}

provider "google" {}

resource "google_compute_instance" "consul" {
  count = "${var.consul_count}"
  name  = "${var.prefix}-consul-${count.index}"

  machine_type = "g1-small"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

output "consul-internal-addresses" {
  value = ["${google_compute_instance.consul.*.network_interface.0.address}"]
}

output "consul-addresses" {
  value = ["${google_compute_instance.consul.*.network_interface.0.access_config.0.assigned_nat_ip}"]
}

resource "google_compute_instance" "database" {
  count = "${var.database_count}"
  name  = "${var.prefix}-database-${count.index}"

  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

output "database-internal-addresses" {
  value = ["${google_compute_instance.database.*.network_interface.0.address}"]
}

output "database-addresses" {
  value = ["${google_compute_instance.database.*.network_interface.0.access_config.0.assigned_nat_ip}"]
}

resource "google_compute_instance" "application" {
  count = "${var.application_count}"
  name  = "${var.prefix}-application-${count.index}"

  tags = ["http-server", "https-server"]

  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

output "application-internal-addresses" {
  value = ["${google_compute_instance.application.*.network_interface.0.address}"]
}

output "application-addresses" {
  value = ["${google_compute_instance.application.*.network_interface.0.access_config.0.assigned_nat_ip}"]
}
